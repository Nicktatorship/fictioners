﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    
    class MainGame
    {
        KeyHandler keyHandler = new KeyHandler();
        Player player = new Player();
        Cartographer atlas = new Cartographer();
        Parser parser = new Parser();
        
        public MainGame() 
        {
            player.Location = DBHandler.Instance.StartingRoom;
            GameLoop();
        }

        void GameLoop()
        {
            string userInput;
            string playerAction;

            do
            {
                Messenger.Instance.ShowLocation(player);
                Messenger.Instance.ShowStatus(player);
                Messenger.Instance.ShowCursor(player);
                
                userInput = Console.ReadLine();
                playerAction = parser.execute(player, userInput);
                GameObject targetObject = parser.getTarget();

                Messenger.Instance.ShowMessage(player, playerAction + " : {0}", targetObject);

                switch (playerAction) {
                    case "stats" : 
                        if (targetObject != null) {
                            Messenger.Instance.ShowQuery(player, targetObject);
                        } else {
                            Messenger.Instance.ShowStats(player);
                        }
                        break;
                    case "showdb": Messenger.Instance.ShowDB(player); break;
                    case "go" :
                        switch (player.actionMove(targetObject))
                        {
                            case -2: Messenger.Instance.ShowMessage(player, "The {0} is locked.", targetObject); break;
                            case 1: Messenger.Instance.ShowMessage(player, "You used the {0} exit.", targetObject);
                                ((Exit) targetObject).notifyContent(player);
                                break;
                        }
                        break;
                    case "look": 
                        if (targetObject != null) 
                        { 
                            Messenger.Instance.ShowDescription(player, targetObject); 
                        } break;
                    case "get":
                        switch (player.actionGet(targetObject)) {
                            case -1: Messenger.Instance.ShowMessage(player, "No object found."); break;
                            case -2: Messenger.Instance.ShowMessage(player, "You cannot take the {0}.", targetObject); break;
                            case -3: Messenger.Instance.ShowMessage(player, "You already have the {0}.", targetObject); break;
                            default: Messenger.Instance.ShowMessage(player, "You take the {0}.", targetObject); break;
                        }
                        break;
                    case "wield":
                        switch (player.actionWield(targetObject))
                        {
                            case -1: Messenger.Instance.ShowMessage(player, "No object found."); break;
                            case -2: Messenger.Instance.ShowMessage(player, "The {0} cannot be wielded.", targetObject); break;
                            case -3: Messenger.Instance.ShowMessage(player, "You do not have the {0}.", targetObject); break;
                            case -4: Messenger.Instance.ShowMessage(player, "You are already wielding the {0}.", targetObject); break;
                            default: Messenger.Instance.ShowMessage(player, "You wield the {0}!", targetObject); break;
                        }
                        break;
                    case "drop":
                        switch (player.actionDrop(targetObject)) {
                            case -1: Messenger.Instance.ShowMessage(player, "No object found."); break;
                            case -2: Messenger.Instance.ShowMessage(player, "You cannot drop the {0}.", targetObject); break;
                            case -3: Messenger.Instance.ShowMessage(player, "You do not have the {0}.", targetObject); break;
                            default: Messenger.Instance.ShowMessage(player, "You drop the {0}.", targetObject); 
                                player.Location.actionGet(targetObject); break;
                        }
                        break;
                }
                
            } while (!playerAction.Equals("quit"));
        }



    }
}
