﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public sealed class DBHandler
    {
        public int NextObject { get; set; }

        private List<GameObject> _db = new List<GameObject>();
        public List<GameObject> GameDB
        {
            get { return _db; }
            set { _db = value; }
        }

        public GameObject StartingRoom { get; set; }

        static readonly DBHandler _instance = new DBHandler();

        public static DBHandler Instance
        {
            get { return _instance; }
        }

        private DBHandler()
        {
        }

        public List<GameObject> getObjectsByType(Type type)
        {
            List<GameObject> objects = new List<GameObject>();
            objects = _db.FindAll(
                   delegate(GameObject g)
                   {
                       return (g.GetType().Equals(type));
                   });
            return objects;
        }

        public List<GameObject> getContents(GameObject obj)
        {
            List<GameObject> objects = new List<GameObject>();
            objects = _db.FindAll(
                   delegate(GameObject g)
                   {
                       return (g.Location == obj);
                   });
            return objects;
        }

        public List<GameObject> getContents(GameObject obj, Type type)
        {
            List<GameObject> objects = new List<GameObject>();
            objects = _db.FindAll(
                   delegate(GameObject g)
                   {
                       return ((g.Location == obj) && (g.GetType().Equals(type)));
                   });
            return objects;
        }

        public Exit getExitByName(String name, GameObject location)
        {
            List<Exit> exits = new List<Exit>();
            foreach (GameObject g in getContents(location, typeof(Exit)))
            {
                if ((g.Identifier.Equals(name, StringComparison.CurrentCultureIgnoreCase)) ||
                    ((g.Alias != null) && (g.Alias.Equals(name, StringComparison.CurrentCultureIgnoreCase))) ||
                    ((name.Length > 4) && (g.Identifier.StartsWith(name, StringComparison.CurrentCultureIgnoreCase)))
                    )
                {
                    return (Exit)g;
                }
            }
            return null;
        }
    }
}
