﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public class Weapon: Item, IWieldable
    {
        public bool Wielded { get; set; }
        public Weapon(string identifier, string description, GameObject location, bool isTakeable = true) : base(identifier, description, location, isTakeable) 
        {
            Wielded = false;
        }
    }
}
