﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public sealed class Messenger
    {
        static readonly Messenger _instance = new Messenger();

        public static Messenger Instance
        {
            get { return _instance; }
        }

        private Messenger()
        {
        }

        public void ShowMessage(Player player, String message)
        {
            Console.WriteLine(message);
        }

        public void ShowMessage(Player player, String message, GameObject g)
        {
            Console.WriteLine(message, (g != null ? g.Identifier : "(null)"));
        }

        public void ShowDescription(Player player, GameObject g)
        {
            Console.WriteLine(g.Description);
        }

        public void ShowStats(Player player)
        {
            foreach (Room r in DBHandler.Instance.getObjectsByType(typeof(Room)))
            {
                Console.WriteLine("Room : {0} : {1}", r.Id, r.Identifier);
                foreach (GameObject g in r.getContents())
                {
                    Console.WriteLine("{0}: {1}", g.GetType(), g.Identifier);
                }
            }
        }

        public void ShowDB(Player player)
        {
            foreach (GameObject g in DBHandler.Instance.GameDB)
            {
                Console.WriteLine("DB# : {0}", g.Id);
                Console.WriteLine("Type: {0}", g.GetType().ToString());
                Console.WriteLine("Ident: {0}", g.Identifier);
                Console.WriteLine("Desc: {0}", g.Description);
                Console.WriteLine("Location: {0}", g.Location.Identifier);
            }
        }

        public void ShowQuery(Player player, GameObject g)
        {
            Console.WriteLine("DB# : {0}", g.Id);
            Console.WriteLine("Type: {0}", g.GetType().ToString());
            Console.WriteLine("Ident: {0}", g.Identifier);
            Console.WriteLine("Desc: {0}", g.Description);
            Console.WriteLine("Location: {0}", g.Location.Identifier);
        }

        public void ShowLocation(Player player) {
            List<Exit> exits = new List<Exit>();
            List<GameObject> objects = new List<GameObject>();

            foreach(GameObject g in player.Location.getContents())
            {
                if (g is Exit)
                {
                    exits.Add((Exit) g);
                } else {
                    objects.Add(g);
                }
            }

            Console.WriteLine(@"---------------------------------
You are in the {0}.", player.Location.Identifier);
            ShowDescription(player, player.Location);

            if (objects.Count() != 0)
            {
                Console.WriteLine("Contents:");
                foreach (GameObject g in objects)
                {
                    Console.WriteLine("{0} : {1}", g.GetType(), g.Identifier);
                }
            }

            if (exits.Count() != 0)
            {
                Console.WriteLine("Exits:");
                foreach (Exit e in exits)
                {
                    Console.WriteLine("<{0}> {1}", (e.Alias != null ? e.Alias : e.Identifier), e.Identifier);
                }
                Console.WriteLine("");
            }

            Console.WriteLine(@"---------------------------------
");


        }
        public void ShowStatus(Player player) {
            Console.WriteLine(@"
---( H:{0}/{1} | E:{2}/{3} | M:{4}/{5} )--
",
                player.CurrentHealth, player.MaximumHealth,
                player.CurrentEnergy, player.MaximumEnergy,
                player.CurrentMana, player.MaximumMana);

        }
        public void ShowCursor(Player player) {
            Console.WriteLine();
            Console.Write(" > ");
        }
    }
}
