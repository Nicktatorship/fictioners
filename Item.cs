﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public class Item : GameObject
    {
        public Item() { }

        public Item(string identifier, string description, GameObject location)
        {
            Identifier = identifier;
            Description = description;
            Location = location;
            Takeable = true;
        }

        public Item(string identifier, string description, GameObject location, bool isTakeable = true)
        {
            Identifier = identifier;
            Description = description;
            Location = location;
            Takeable = isTakeable;
        }
    }
}
