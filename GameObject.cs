﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public class GameObject
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public bool Takeable { get; set; }
        public bool Dropable { get; set; }
        public GameObject Location { get; set; }

        // Constructors for GameObject class

        public GameObject()
        {
            Id = DBHandler.Instance.NextObject++;
            Identifier = "Unnamed";
            DBHandler.Instance.GameDB.Add(this);
            Takeable = false;
        }

        public GameObject(string identifier="Unnamed", string description="Not set.")
        {
            Id = DBHandler.Instance.NextObject++;
            Identifier = identifier;
            Description = description;
            DBHandler.Instance.GameDB.Add(this);
            Takeable = false;
        }

        public bool hasContents()
        {
            return (DBHandler.Instance.getContents(this).Count() != 0);
        }

        public List<GameObject> getContents()
        {
            return DBHandler.Instance.getContents(this);
        }

        public void notifyContent(GameObject g)
        {
        }


        public int actionGet(GameObject g)
        /// Returns 1 for success
        /// Returns -1 for missing object
        /// Returns -2 for untakeable object
        /// Returns -3 for object in inventory
        {
            if (g != null)
            {
                if (this.GetType() != typeof(Player))
                {
                    g.Location = this;
                }
                else if (g.Location == this)
                {
                    return -3;
                }
                else if (!g.Takeable)
                {
                    return -2;
                }
                else
                {
                    g.Location = this;
                    return 1;
                }
            }

            return -1;
        }
        public int actionDrop(GameObject g)
        /// Returns 1 for success
        /// Returns -1 for missing object
        /// Returns -2 for undroppable object
        /// Returns -3 for object not in inventory
        {
            if (g != null)
            {
                if (this.GetType() != typeof(Player))
                {
                    g.Location = this.Location;
                }
                else if (g.Location != this)
                {
                    return -3;
                }
                else if (!g.Dropable)
                {
                    return -2;
                }
                else
                {
                    IWieldable obj = g as IWieldable;
                    if (obj != null)
                    {
                        obj.Wielded = false;
                    }
                    g.Location = this.Location;
                    return 1;
                }
            }

            return -1;
        }

        public int actionWield(GameObject g)
        /// Returns 1 for success
        /// Returns -1 for missing object
        /// Returns -2 for unwieldable object
        /// Returns -3 for object not in inventory
        {
            if (g != null)
            {
                if (g.Location != this)
                {
                    return -3;
                } else {
                    IWieldable obj = g as IWieldable;
                    if (obj != null) {
                        if (obj.Wielded)
                        {
                            return -4;
                        }
                        else
                        {
                            obj.Wielded = true;
                            return 1;
                        }
                    } else {
                        return -2;
                    }
                }
            }

            return -1;
        }

        public int actionMove(GameObject g)
        /// Returns 1 for success
        /// Returns -1 for missing exit
        /// Returns -2 for locked exit
        {
            if (g != null)
            {
                Exit e = (Exit) g;
                if (!e.Locked)
                {
                    this.Location = e;
                    return 1;
                }
                else
                {
                    return -2;
                }
            }

            return -1;
        }
    }
}
