﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    class Cartographer
    {
        public Cartographer()
        {
            generateMap();
        }

        public void generateMap() 
        {
            Room dungeon = new Room("Dungeon", "You are in a dungeon. There is some light coming from the wall.");
            Room outside = new Room("Outside", "You are outside the dungeon, amongst many trees.");
            Room cellar = new Room("Cellar", "You are in the musty cellar.");
            Room street = new Room("Street", "You are standing on the street.");
            Room oldwell = new Room("Old Well", "There is an old well at the top of the hill.");
            Room shop = new Room("Shop", "You are inside the general store.");

            Item bucket = new Item("Bucket", "An empty bucket.", oldwell, true);
            Item barrel = new Item("Large Barrel", "A large oak barrel, filled with wine.", cellar, false);

            Weapon sword = new Weapon("Wooden Sword", "A worn wooden sword.", dungeon, true);
            Weapon gun = new Weapon("Gun", "A solid-looking gun.", shop, true);

            Character merchant = new Character("Merchant", "A short, round man with glasses.", shop);

            Exit passage = new Exit("Passage", "A secret passageway", cellar, street, "pas");

            addExit(dungeon, outside, "Tunnel", "The tunnel seems to lead outside.");
            addExit(outside, dungeon, "Tunnel", "The tunnel leads into the dungeon.");

            addExit(dungeon, cellar, "Hatch", "There is a hatch leading up out of the cell.");
            addExit(cellar, dungeon, "Trapdoor", "There is a trapdoor leading down into darkness.");

            addExit(outside, street, "South", "The trees started to clear away toward the south.");
            addExit(street, outside, "North", "There is a walkway leading north, away from the road.");

            addExit(outside, oldwell, "North", "There is a hill to the north.");
            addExit(oldwell, outside, "South", "There is a bunch of trees further down the hill.");
            
            addExit(street, shop, "West", "There is a general store on the western edge of the road.");
            addExit(shop, street, "Out", "There is a path leading out of the shop.");

            DBHandler.Instance.StartingRoom = dungeon;

        }

        private void addExit(Room source, Room destination, string identifier, string description) 
        {
            Exit e = new Exit(identifier, description);
            e.Destination = destination;
            e.Location = source;
        }

    }
}
