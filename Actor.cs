﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public class Actor : GameObject
    {
        public int Cunning { get; set; }
        public int Brawn { get; set; }
        public int Wit { get; set; }

        public int CurrentHealth { get; set; }
        public int CurrentMana { get; set; }
        public int CurrentEnergy { get; set; }

        public int MaximumHealth { get; set; }
        public int MaximumMana { get; set; }
        public int MaximumEnergy { get; set; }



        public Actor() {}

        public Actor(string identifier, string description, GameObject location) : base(identifier, description) 
        {
            Location = location;
            
            Cunning = 1;
            Brawn = 1;
            Wit = 1;
            CurrentHealth = 1;
            MaximumHealth = 1;
            CurrentMana = 0;
            MaximumMana = 0;
            CurrentEnergy = 1;
            MaximumEnergy = 1;
        }
    }

}
