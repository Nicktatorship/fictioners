﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public class Exit: GameObject
    {
        public GameObject Destination { get; set; }
        public bool Locked { get; set; }
        public Exit(string identifier, string description)
        {
            Identifier = identifier;
            Description = description;
            Locked = false;
        }
        public Exit(string identifier, string description, GameObject source, GameObject dest, string alias)
        {
            Identifier = identifier;
            Description = description;
            Location = source;
            Destination = dest;
            Alias = alias;
            Locked = false;
        }

        new public void notifyContent(GameObject g)
        {
            g.Location = this.Destination;
            if (g is Player)
            {
                Messenger.Instance.ShowMessage((Player)g, "Transit through {0}.", this);
            }
        }



    }
}
