﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World1
{
    public interface IWieldable
    {
        bool Wielded { get; set;}
        int actionWield(GameObject g);

    }
}
